resource "aws_s3_bucket" "jocasmen-test" {
  bucket = var.s3_name
}

resource "aws_s3_bucket_acl" "jocasmen-test" {
  bucket = aws_s3_bucket.jocasmen-test.id
  acl    = "private"
}

resource "aws_s3_bucket_website_configuration" "jocasmen-test" {
  bucket = aws_s3_bucket.jocasmen-test.bucket

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "index.html"
  }

}

resource "aws_s3_bucket_policy" "jocasmen-test" {
  bucket = aws_s3_bucket.jocasmen-test.id

  policy = jsonencode({
    Version = "2012-10-17"
    Id      = "AllowGetObjects"
    Statement = [
      {
        Sid       = "AllowPublic"
        Effect    = "Allow"
        Principal = "*"
        Action    = "s3:GetObject"
        Resource  = "${aws_s3_bucket.jocasmen-test.arn}/**"
      }
    ]
  })
}